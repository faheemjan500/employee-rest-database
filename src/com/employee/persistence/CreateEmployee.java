/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.employee.persistence;

import com.employee.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CreateEmployee {

  public static void main(String[] args) {

    final EntityManagerFactory entityManagerFactory =
        Persistence.createEntityManagerFactory("tutorialspoint_JPA_Eclipselink");

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();

    final Employee employee1 = new Employee();

    employee1.setEid(1551);
    employee1.setEname("madoba");
    employee1.setSalary(7100);
    employee1.setDeg("Cleaner");

    entityManager.persist(employee1);
    entityManager.getTransaction().commit();

    entityManager.close();
    entityManagerFactory.close();
  }
}