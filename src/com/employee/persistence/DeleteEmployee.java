/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package com.employee.persistence;

import com.employee.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DeleteEmployee {
  public static void main(String[] args) {

    final EntityManagerFactory emfactory =
        Persistence.createEntityManagerFactory("tutorialspoint_JPA_Eclipselink");
    final EntityManager entitymanager = emfactory.createEntityManager();
    entitymanager.getTransaction().begin();

    final Employee employee = entitymanager.find(Employee.class, 4201);
    entitymanager.remove(employee);
    entitymanager.getTransaction().commit();
    entitymanager.close();
    emfactory.close();
  }
}