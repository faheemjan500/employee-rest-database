/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.employee.persistence;

import com.employee.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FindEmployee {
  public static void main(String[] args) {

    final EntityManagerFactory emfactory =
        Persistence.createEntityManagerFactory("tutorialspoint_JPA_Eclipselink");
    final EntityManager entitymanager = emfactory.createEntityManager();
    final Employee employee = entitymanager.find(Employee.class, 1130);

    System.out.println("employee ID = " + employee.getEid());
    System.out.println("employee NAME = " + employee.getEname());
    System.out.println("employee SALARY = " + employee.getSalary());
    System.out.println("employee DESIGNATION = " + employee.getDeg());
  }
}