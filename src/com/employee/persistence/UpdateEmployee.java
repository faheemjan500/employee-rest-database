/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.employee.persistence;

import com.employee.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UpdateEmployee {
  public static void main(String[] args) {
    final EntityManagerFactory emfactory =
        Persistence.createEntityManagerFactory("tutorialspoint_JPA_Eclipselink");

    final EntityManager entitymanager = emfactory.createEntityManager();
    entitymanager.getTransaction().begin();
    final Employee employee = entitymanager.find(Employee.class, 7830);

    // before update
    System.out.println(employee);
    employee.setSalary(46000);
    entitymanager.getTransaction().commit();

    // after update
    System.out.println(employee);
    entitymanager.close();
    emfactory.close();
  }
}
