/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.employee.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table()
public class Employee {

  @Id
  @GeneratedValue

  private int employeeId;
  private String employeeName;
  private double salary;
  private String deg;

  public Employee() {
    super();
  }

  public Employee(int eid, String ename, double salary, String deg) {
    super();
    this.employeeId = eid;
    this.employeeName = ename;
    this.salary = salary;
    this.deg = deg;
  }

  public String getDeg() {
    return deg;
  }

  public int getEid() {
    return employeeId;
  }

  public String getEname() {
    return employeeName;
  }

  public double getSalary() {
    return salary;
  }

  public void setDeg(String deg) {
    this.deg = deg;
  }

  public void setEid(int eid) {
    this.employeeId = eid;
  }

  public void setEname(String ename) {
    this.employeeName = ename;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }

  @Override
  public String toString() {
    return "Employee [eid=" + employeeId + ", ename=" + employeeName + ", salary=" + salary
        + ", deg=" + deg + "]";
  }

}