/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.employee.service;

import com.employee.entity.Employee;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/employee")
public class EmployeeService {
  private final EntityManagerFactory entityManagerFactory =
      Persistence.createEntityManagerFactory("tutorialspoint_JPA_Eclipselink");
  private final EntityManager entityManager = entityManagerFactory.createEntityManager();
  List<Employee> employeesList = new ArrayList<>();

  @POST
  @Path("/createEmployee")
  @Produces({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  @Consumes({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  public void createEmployee(Employee employee1) {
    entityManager.getTransaction().begin();
    entityManager.persist(employee1);
    entityManager.getTransaction().commit();
    entityManager.close();
    entityManagerFactory.close();

  }

  @DELETE
  @Path("/deleteEmployee/{employeeId}")
  @Produces({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  public void deleteEmployee(@PathParam("employeeId") int id) {
    entityManager.getTransaction().begin();
    final Employee employee = entityManager.find(Employee.class, id);
    entityManager.remove(employee);
    entityManager.getTransaction().commit();
    entityManager.close();
    entityManagerFactory.close();
  }

  @GET
  @Path("/findEmployee/{employeeId}")
  @Produces({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  public Employee findEmployee(@PathParam("employeeId") int id) {
    return entityManager.find(Employee.class, id);

  }

  @GET
  @Path("/getAllEmployees")
  @Produces({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  @SuppressWarnings("unchecked")
  public List<Employee> getAllEmployees() {
    entityManager.getTransaction().begin();
    employeesList = entityManager.createQuery("SELECT e FROM Employee e").getResultList();
    entityManager.getTransaction().commit();
    entityManager.close();
    entityManagerFactory.close();
    return employeesList;
  }

  @GET
  @Path("/specificEmployees")
  @Produces({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  @SuppressWarnings("unchecked")
  public List<Employee> getEmployessWithSpecificRangeOfSalaries() {
    entityManager.getTransaction().begin();
    final javax.persistence.Query query = entityManager.createQuery(
        "Select e " + "from Employee e " + "where e.salary " + "Between 6030 and 46000");
    employeesList = query.getResultList();
    entityManager.getTransaction().commit();
    entityManager.close();
    entityManagerFactory.close();
    return employeesList;
  }

}
